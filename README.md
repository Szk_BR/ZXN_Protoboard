# ZXN Protoboard

A multi-purpose, hardware developer-oriented DIY PCB with buffered I/O expansion/extension and a breadboard/prototyping area, suited to all Sinclair ZX Spectrum models. For the ZX Next, it adds bus level-shifting and (de)inverting the BUSREQ line.

<img src=Pictures/ZXN_Protoboard_v1.32_by_JLCPCB.JPG>
